

Supported platforms and compilers
=================================

Many tests fail using Intel 2015. Therefore we currently do not recommend to
compile Dalton |version| using Intel 2015.
